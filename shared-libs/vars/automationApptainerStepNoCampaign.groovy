#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: 'ecalgit-lxplus', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        s_script = "apptainer exec --cleanenv -B /run -B /cvmfs -B /eos -B /afs $image /bin/bash -c 'echo '$PASSWORD' | kinit -V $USERNAME; $setup; export USER=$USERNAME; export ECAL_INFLUXDB_USER=ecalgit; export X509_USER_PROXY=$HOME/grid_proxy.x509; $script'"
        sh(script: s_script)
    }
}

