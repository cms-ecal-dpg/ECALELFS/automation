#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: 'ecalgit-lxplus', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        // Wrap the script into a singularity exec
        get_campaigns = "`if [ $campaign == 'all' ]; then ecalrunctrl.py --db $dbinstance list-campaigns -m -a; else echo $campaign | tr \",\" \"\\n\"; fi`"
        s_script = "apptainer exec --cleanenv -B /run -B /cvmfs -B /eos -B /afs $image /bin/bash -c 'echo '$PASSWORD' | kinit -V $USERNAME; $setup; export USER=$USERNAME; export ECAL_INFLUXDB_USER=ecalgit; export X509_USER_PROXY=$HOME/grid_proxy.x509; for CAMPAIGN in $get_campaigns \n do $script \n done'"
        sh(script: s_script)
    }
}

