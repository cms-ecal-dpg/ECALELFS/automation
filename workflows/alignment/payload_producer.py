#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
import numpy as np
from multiprocessing import Pool
from typing import Optional, Dict, List, Any
from os import remove, path
from ecalautoctrl import HandlerBase, JobCtrl, RunCtrl, process_by_intlumi, prev_task_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

class AlignMediateHandler(HandlerBase):
    """
    Run all intermediate steps of ECAL alignment.
     - merge alignment coefficient txt files
     - get current alignment conditions from the GT
     - add new alignment coefficients to the old ones
     - Produce sqlite files 

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if not deps_tasks:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

    def merge_files(self, outfilepath: str):
        """
        Merge alignment txt files. There is one txt file per SM/Dee. 
        Merge these to one txt each for EB and EE.
        :param outfilepath: path to the eos directory containing the txt files        
        """
        with open(outfilepath+'/EBAlignmentCoefficients_merged.txt', 'w') as outfile:  # Use 'w' mode for overwriting
            for i in range(0,36):
                with open(outfilepath+'/EBAlignmentCoefficients_%d.txt'%i, 'r') as infile:
                    outfile.write(infile.read())
        self.log.info('EB alignment coefficient txt files merged')
    
        with open(outfilepath+'/EEAlignmentCoefficients_merged.txt', 'w') as outfile:  # Use 'w' mode for overwriting
            for i in range(0,4):
                with open(outfilepath+'/EEAlignmentCoefficients_%d.txt'%i, 'r') as infile:
                    outfile.write(infile.read())
        self.log.info('EE alignment coefficient txt files merged')
        #ret = subprocess.run(f'python3 {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/merge_txt_files.py {outfilepath}',
        #                               shell=True, capture_output=True)                         
    
    def get_current_tag(self, gt, outfilepath: str):
        """
        Gets the current ECAL alignment tag from the GT and converts it into 2 txt files (one for EB and another for EE).  
       
        :param gt: current global tag
        :param outfilepath: path to the eos directory containing the txt files        
        """
        ret = subprocess.run(f'python3 {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/sqlite_to_txt.py {gt}',
                                         shell=True, capture_output=True)
        shutil.copy('EBAlignmentCoefficients_old.txt', outfilepath)
        shutil.copy('EEAlignmentCoefficients_old.txt', outfilepath)
        os.remove('EBAlignmentCoefficients_old.txt')
        os.remove('EEAlignmentCoefficients_old.txt')
        self.log.info('Got current alignment conditions from the GT')

        
    def process_data(self, groups: List[Dict]):
        """
        Execute the following steps:
        - merging alignment txt files 
        - create a similar txt file from the current alignment tag 
        - Combine the new alignment conditions with the current one
        - Create sqlite files 
        
        :param groups: groups of run to be processed.
        """

        for group in groups:
            self.log.info('Processing run(s): '+','.join([r['run_number'] for r in group]))
            # reset output
            outputs = {}
            # gather master run details
            mrun = group[-1]
            run = mrun['run_number']
            fill = mrun['fill']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : mrun['fill']},
                            dbname=self.opts.dbname)
            repr = (mrun[self.task] == 'reprocess')
            if not jctrl.taskExist() or repr:
                jctrl.createTask(jids=[0],
                                 recreate=repr,
                                 fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
            try:
                # set run status
                self.rctrl.updateStatus(run=run, status={self.task : 'processing'})
                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                jctrl.running(jid=0)
                # get files
                files = self.get_files(group)
                #self.log.info(files)
                # set output name
                eospath = path.abspath(path.dirname(list(files)[0]))
                self.log.info(f'eospath: {eospath}')
                # merge single job / single runs out files
                self.merge_files(outfilepath=eospath)
                # get the current alignment tag and convert it to another txt file
                global_tag = mrun['globaltag']
                self.log.info(f'GlobalTag: {global_tag}')
                self.get_current_tag(gt=global_tag, outfilepath=eospath)
                # combine alignment txt files
                ret_combine_EB = subprocess.run(f'CombineRotoTraslations {eospath}/EBAlignmentCoefficients_old.txt {eospath}/EBAlignmentCoefficients_merged.txt {eospath}/EBAlignmentCoefficients_new.txt',
                                        cwd=f'{os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/',
                                        shell=True, capture_output=True)
                if ret_combine_EB.returncode == 0:
                    self.log.info(ret_combine_EB.stdout.decode().strip())
                else:
                    self.log.info(ret_combine_EB.stdout.decode().strip())
                    self.log.info(ret_combine_EB.stderr.decode().strip())
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed combining old EB alignment coefficients with the new ones for run {r["run_number"]}')
                    continue
                self.log.info('Combined old EB alignment coefficients with the new ones')

                ret_combine_EE = subprocess.run(f'CombineRotoTraslations {eospath}/EEAlignmentCoefficients_old.txt {eospath}/EEAlignmentCoefficients_merged.txt {eospath}/EEAlignmentCoefficients_new.txt',
                                        cwd=f'{os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/',
                                        shell=True, capture_output=True)
                if ret_combine_EE.returncode == 0:
                    self.log.info(ret_combine_EE.stdout.decode().strip())
                else:
                    self.log.info(ret_combine_EE.stdout.decode().strip())
                    self.log.info(ret_combine_EE.stderr.decode().strip())
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed combining old EE alignment coefficients with the new ones for run {r["run_number"]}')
                    continue
                self.log.info('Combined old EE alignment coefficients with the new ones')
                
                # create sqlite files (new alignment conditions) 
                sqlitefile_eb = 'EBAlign_test.db'
                if os.path.isfile(sqlitefile_eb):
                    os.remove(sqlitefile_eb)
                ret_newtag_EB = subprocess.run(f'cmsRun {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/test/dbEcalAlignment/copyFileAlignEB_cfg.py inFile={eospath}/EBAlignmentCoefficients_new.txt',
                                         shell=True, capture_output=True)
                if ret_newtag_EB.returncode == 0:
                    self.log.info(ret_newtag_EB.stdout.decode().strip())
                else:
                    self.log.info(ret_newtag_EB.stdout.decode().strip())
                    self.log.info(ret_newtag_EB.stderr.decode().strip())
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing local EB alignment tag for run {r["run_number"]}')
                    continue
                shutil.copy(sqlitefile_eb, eospath)
                # place a link to the sqlite file in directories for every run in the group except the main one
                for r in group[:-1]:
                    eospath_grp_run = os.path.split(eospath)[0] + f"/{r['run_number']}"
                    if not os.path.isdir(eospath_grp_run):
                        os.makedirs(eospath_grp_run)
                    linkpath = eospath_grp_run + f"/{sqlitefile_eb}"
                    if os.path.islink(linkpath):
                        os.remove(linkpath)
                    os.symlink(eospath + f"/{sqlitefile_eb}", linkpath)
                os.remove(sqlitefile_eb)
                self.log.info('produced local EB alignment tag')

                sqlitefile_ee = 'EEAlign_test.db'
                if os.path.isfile(sqlitefile_ee):
                    os.remove(sqlitefile_ee)
                ret_newtag_EE = subprocess.run(f'cmsRun {os.environ["CMSSW_BASE"]}/src/EcalValidation/EcalAlignment/test/dbEcalAlignment/copyFileAlignEE_cfg.py inFile={eospath}/EEAlignmentCoefficients_new.txt',
                                         shell=True, capture_output=True)
                if ret_newtag_EE.returncode == 0:
                    self.log.info(ret_newtag_EE.stdout.decode().strip())
                else:
                    self.log.info(ret_newtag_EE.stdout.decode().strip())
                    self.log.info(ret_newtag_EE.stderr.decode().strip())
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing local EE alignment tag for run {r["run_number"]}')
                    continue
                shutil.copy(sqlitefile_ee, eospath)
                # place a link to the sqlite file in directories for every run in the group except the main one
                for r in group[:-1]:
                    eospath_grp_run = os.path.split(eospath)[0] + f"/{r['run_number']}"
                    if not os.path.isdir(eospath_grp_run):
                        os.makedirs(eospath_grp_run)
                    linkpath = eospath_grp_run + f"/{sqlitefile_ee}"
                    if os.path.islink(linkpath):
                        os.remove(linkpath)
                    os.symlink(eospath + f"/{sqlitefile_ee}", linkpath)
                os.remove(sqlitefile_ee)
                self.log.info('produced local EE alignment tag')

                # mark as completed
                outputs.update({'sqlite': eospath + f'/{sqlitefile_eb},' + eospath + f'/{sqlitefile_ee}'})
                jctrl.done(jid=0, fields=outputs)

            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed processing merging step for run {run}: {e}')
        
    def submit(self):
        """
        Execute the local merging step. 
        Reprocess re-injected runs as well.
        """

        # process new runs / fill
        self.process_data(groups=self.groups())

        return 0

    def resubmit(self):
        """
        Resubmit runs with failed jobs
        """

        # get runs in status processing and merged
        runs_processing = self.rctrl.getRuns(status = {self.task : 'processing'})
        runs_merged = self.rctrl.getRuns(status = {self.task : 'merged'})

        run_groups = []
        # check if any job failed before resubmitting
        for run_dict in runs_processing:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                # build group of processing runs with corresponding merged runs
                job = jctrl.getJob(0, last=True)[0]
                if 'group' in job:
                    grouped_runs = jctrl.getJob(0, last=True)[0]['group'].split(',')
                else:
                    grouped_runs = []
                run_group = []
                if len(grouped_runs) > 0:
                    for run_merged in runs_merged:
                        if run_merged['run_number'] in grouped_runs:
                            run_group.append(run_merged)
                # append processing run to the group as the last run
                run_group.append(run_dict)
                run_groups.append(run_group)

        self.process_data(groups=run_groups)

        return 0

@prev_task_data_source
@process_by_intlumi(target=2000)
class AlignMediateHandlerRun(AlignMediateHandler):
    """
    Run ECAL alignment steps between minimization and rereco.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=[],
                 **kwargs):        
        super().__init__(task=task,
                         prev_input=prev_input,
                         deps_tasks=deps_tasks,
                         **kwargs)
    
if __name__ == '__main__':
    handler = AlignMediateHandlerRun(task='alignment-payload-producer',
                                prev_input='alignment-align')

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(AlignMediateHandlerRun)
