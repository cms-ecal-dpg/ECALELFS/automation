#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
PART=${7}
SUBPART=${8}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/
export EOS_MGM_URL=root://eoscms.cern.ch

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here, this can be a CMSSW job or anything else (cmsRun is just an example)
mkdir output
if [ "$PART" == "0" ]
then
    EB_Alignment_RotoTraslation_singleSM $CMSSW_BASE/src/EcalValidation/EcalAlignment/test/align/AlignEB_Zee_cfg.py inputFiles=$INFILE iSM=$SUBPART
    RETCMSSW=$?
else
    EE_Alignment_RotoTraslation_singleSM $CMSSW_BASE/src/EcalValidation/EcalAlignment/test/align/AlignEE_Zee_cfg.py inputFiles=$INFILE iDee=$SUBPART
    RETCMSSW=$?
fi

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location
    eos mkdir -p $EOSDIR
    OFILE=`ls output/*txt`
    OFILEBASE=`basename $OFILE`
    xrdcp -f $OFILE $EOS_MGM_URL/$EOSDIR/$OFILEBASE
    RETCOPY=$?
    OFILE=$EOSDIR/$OFILEBASE
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
