# Renew the ecalgit GRID proxy 

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/ecal-grid-proxy/) 
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/grid-proxy)

## Workflow structure
Generate a new GRID proxy for the ecalgit account


