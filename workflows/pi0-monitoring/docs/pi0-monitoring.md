# Pi0 invariant mass monitoring

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/pi0-reco-prod/) 
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/pi0-monitoring)

## Workflow structure
The Pi0 monitoring workflows runs on top of a dedicated AlCa stream:

- `/AlCaP0/*/RAW`

The first task reconstruct pi0 candidates from the stream, subsequent tasks produce the monitoring plots: per fill and comulative.

The automation workflow structure can be seen in the diagram below.

![wflow-fig](pi0-monitoring-wflow.png)




