#!/usr/bin/env python3
import sys
import os
import subprocess
from importlib import util
# import ROOT if possible (use mock import for docs generation)
try:
    if util.find_spec('ROOT'):
        import ROOT
except:
    from unittest.mock import MagicMock, patch
    my_conddb = MagicMock()
    patch.dict("sys.modules", fake_root=my_conddb).start()
    import fake_root as ROOT
from typing import Optional, Dict, List
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_fill
from ecalautoctrl.CMSTools import QueryOMS
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase
from omsapi import OMSAPI
from condiovtools import *

@prev_task_data_source
@process_by_fill(fill_complete=True)
class Pi0MonHandler(HandlerBase):
    """
    Execute all the steps to generate the pi0 monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')
        
    def process_data(self, groups: List[Dict]):
        """
        Read the reco files and produce the monitoring plots.
        """

        macro = './finalTimeVariationPlot.C'
        ROOT.gROOT.LoadMacro(macro)

        omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
        omsapi.auth_oidc('ecalgit-omsapi', 'KXbuy4vxiETBc5C7FwteQxAF3X1irilx')
        omsquery = QueryOMS()

        for i, group in enumerate(groups):
            # only process up to 10 groups in one Jenkins build to allow downstream tasks in other runs to be processed as well
            if i > 9:
                break

            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number' : run['run_number'], 'fill' : run['fill']},
                                dbname=self.opts.dbname)

                repr = (run[self.task] == 'reprocess')
                if not jctrl.taskExist() or repr:
                    jctrl.createTask(jids=[0],
                                     recreate=repr,
                                     fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
                try:
                    jctrl.running(jid=0)
                    self.log.info(f'Processing fill {run["fill"]}.')
                    self.rctrl.updateStatus(run=run['run_number'], status={self.task : 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                    eosdir = os.path.abspath(self.opts.eosplots+f'/{run["fill"]}/')
                    plotsurl = self.opts.plotsurl+f'/{run["fill"]}/'
                    os.makedirs(eosdir, exist_ok=True)
                    # run the splitting and mass fits 
                    self.log.info(f'Running the splitting and pi0 mass fits.')
                    # exe = os.environ['CMSSW_BASE']+'/src/CalibCode/submit/monitoring/collectEventInTimeFrame.py'
                    exe = 'collectEventInTimeFrame.py'
                    cmd = ['python3', os.path.abspath(exe), '--inFile', ','.join(fdict),
                           '--outFile', os.path.abspath(f'{eosdir}/pi0_fitMassInfo_{run["fill"]}.txt')]
                    self.log.info(' '.join(cmd))
                    ret = subprocess.run(cmd, capture_output=True)
                    if ret.returncode == 0:
                        self.log.info(ret.stdout.decode().strip())
                    else:
                        jctrl.failed(jid=0)
                        self.log.info(ret.stdout.decode().strip())
                        self.log.info(ret.stderr.decode().strip())
                        self.log.error(f'Failed processing collectEventInTimeFrame.py')
                        return None

                    self.log.info(f'Writing file-list file.')
                    with open("timeVariationFileList.txt", "w") as finalin:
                        finalin.write('#TextFile,Label,Color\n')
                        finalin.write(f'{eosdir}/pi0_fitMassInfo_{run["fill"]}.txt,With Light Monitoring Correction,417')

                    self.log.info(f'Getting runs for conditions IOVs from CondDB.')
                    timingRcdLabel = 'CC'
                    gt = run["globaltag"]
                    # tags for which IOV updates can be shown in the plot
                    psiovruns = get_cond_iov_runs(get_tag(gt, "EcalPulseShapesRcd"), "EcalCond", omsquery, omsapi)
                    iciovruns = get_cond_iov_runs(get_tag(gt, "EcalIntercalibConstantsRcd"), "EcalCond", omsquery, omsapi)
                    #timeiovruns = get_cond_iov_runs(get_tag(gt, "EcalTimeCalibConstantsRcd", timingRcdLabel), "EcalCond", omsquery, omsapi)
                    ebaligniovruns = get_cond_iov_runs(get_tag(gt, "EBAlignmentRcd"), "Alignments", omsquery, omsapi)
                    eealigniovruns = get_cond_iov_runs(get_tag(gt, "EEAlignmentRcd"), "Alignments", omsquery, omsapi)
                    esaligniovruns = get_cond_iov_runs(get_tag(gt, "ESAlignmentRcd"), "Alignments", omsquery, omsapi)
                    pediovruns = get_cond_iov_runs(get_tag(gt, "EcalPedestalsRcd"), "EcalCond", omsquery, omsapi)

                    # tags for which IOV updates are shown in the plot
                    tagiovs = {"PS":psiovruns,
                               #"T_{" + timingRcdLabel + "}":timeiovruns,
                               "IC":iciovruns,
                               "A^{EB}":ebaligniovruns,
                               "A^{EE}":eealigniovruns,
                               "A^{ES}":esaligniovruns,
                               "P":pediovruns
                              }

                    runcondstrs = merge_iovruns(tagiovs)
                    iovruns = [r for r in sorted(runcondstrs) if r > 347000]
                    runstrs, run_start_times = get_run_info(iovruns, omsquery, omsapi)
                    condstrs = ["{0}-{1}".format(runcondstrs[int(r)], r) for r in runstrs]

                    self.log.info(f'Generating pi0 mass time variation plots for fill {run["fill"]}.')
                    ROOT.finalTimeVariationPlot("timeVariationFileList.txt", eosdir+'/', False, condstrs, run_start_times, [int(run["endtime"][:4])])

                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output' : f'{eosdir}/pi0_fitMassInfo_{run["fill"]}.txt',
                        'plots' : plotsurl})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                    continue
    
    def submit(self):
        """
        Produce per fill pi0 mass monitoring plots
        """

        # process new runs / fill
        self.process_data(groups=self.groups())

        return 0

    def resubmit(self):
        """
        Resubmit fills with failed jobs
        """

        # get runs in status processing and merged
        runs_processing = self.rctrl.getRuns(status = {self.task : 'processing'})
        runs_merged = self.rctrl.getRuns(status = {self.task : 'merged'})

        run_groups = []
        # check if any job failed before resubmitting
        for run_dict in runs_processing:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                # build group of processing runs with corresponding merged runs
                job = jctrl.getJob(0, last=True)[0]
                if 'group' in job:
                    grouped_runs = jctrl.getJob(0, last=True)[0]['group'].split(',')
                else:
                    grouped_runs = []
                run_group = []
                if len(grouped_runs) > 0:
                    for run_merged in runs_merged:
                        if run_merged['run_number'] in grouped_runs:
                            run_group.append(run_merged)
                # append processing run to the group as the last run
                run_group.append(run_dict)
                run_groups.append(run_group)

        self.process_data(groups=run_groups)

        return 0

if __name__ == '__main__':
    handler = Pi0MonHandler(task='pi0-mon',
                            deps_tasks=['pi0-reco'],
                            prev_input='pi0-reco')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(Pi0MonHandler)
    
