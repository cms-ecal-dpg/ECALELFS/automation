#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, CondDBLockGT, T0ProcDatasetLock, T0FCSRLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    laser_ped_lock = CondDBLockGT(records=['EcalLaserAPDPNRatiosRcd', 'EcalPedestalsRcd'])
    t0lock = T0ProcDatasetLock(dataset='/AlCaPhiSym', stage='Repack')
    t0_fcsr_lock = T0FCSRLock()
    
    handler = HTCHandlerByRunDBS(task='phisym-reco',
                                 dsetname='/AlCaPhiSym/*/RAW',
                                 locks=[laser_ped_lock, t0lock, t0_fcsr_lock])

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
    
