#!/usr/bin/env python3
import sys
import subprocess
import json
import pickle
import uproot

jobid = sys.argv[1]
eos_dir = sys.argv[2]

proc = subprocess.Popen(
    "echo $CMSSW_BASE", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
)

out, err = proc.communicate()
CMSSW_BASE = out.decode("utf-8").split("\n")[0]


with open("splitting.json") as fi:
    job_dictionary = json.load(fi)

result_dict = {}

harness_ranges = job_dictionary[jobid]
for harness_range in harness_ranges:
    etamin = harness_range[0]
    etamax = harness_range[1]
    phimin = harness_range[2]
    phimax = harness_range[3]

    with open("config.cfg") as fi:
        replaced_contents = fi.read()
        replaced_contents = replaced_contents.replace("CMSSW_BASE", CMSSW_BASE)
        replaced_contents = replaced_contents.replace("IETAMIN", str(etamin))
        replaced_contents = replaced_contents.replace("IETAMAX", str(etamax))
        replaced_contents = replaced_contents.replace("IPHIMIN", str(phimin))
        replaced_contents = replaced_contents.replace("IPHIMAX", str(phimax))

    with open("config_tmp.cfg", "w") as fo:
        fo.write(replaced_contents)

    ret = subprocess.run(
        "$CMSSW_BASE/src/Eop_framework/bin/LaserMonitoring.exe --cfg config_tmp.cfg --scaleMonitor",  # noqa E501
        shell=True,
    )
    if ret.returncode != 0:
        print("Error in laser monitoring")
        sys.exit(ret.returncode)

    f = uproot.open("output.root")
    result_dict[f"IEta_{etamin}_{etamax}_IPhi_{phimin}_{phimax}"] = []
    d = result_dict[f"IEta_{etamin}_{etamax}_IPhi_{phimin}_{phimax}"]
    if len(f["prompt_monitoring"]["scale_Eop_median"].array()) == 0:
        d.append(1.0)
        d.append(1.0)
    else:
        d.append(f["prompt_monitoring"]["scale_Eop_median"].array()[0])
        d.append(f["prompt_monitoring"]["scale_unc_Eop_median"].array()[0])
    f.close()


with open(f"results_{jobid}.json", "w") as file:
    json.dump(result_dict, file, indent=2)

with open(f"results_{jobid}.pkl", "wb") as file:
    pickle.dump(result_dict, file)

command = f"cp results_* {eos_dir}/"
ret = subprocess.run(command, shell=True)


if ret.returncode != 0:
    print("Error in copying files")
    sys.exit(ret.returncode)
sys.exit(0)
