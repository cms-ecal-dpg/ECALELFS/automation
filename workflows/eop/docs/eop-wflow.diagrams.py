from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: eop", filename="eop-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("eop-mon", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        runmon = singleJob("EopHandler \n @prev_task_data_source \n @process_by_intlumi(target=2000)")
        mon_files = file("runeop.py \n script.py")
    with Cluster("eop-cml", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        runcml = singleJob("EopCmlHandler \n @prev_task_data_source \n @process_by_intlumi(target=2000)")
        cml_files = file("runeop-cml.py")

    runmon >> runcml
