Automation workflow docs: Eop workflow
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runeop.py
============

.. argparse::
   :filename: ../runeop.py
   :func: get_opts
   :prog: runeop.py

runeop-cml.py
============

.. argparse::
   :filename: ../runeop-cml.py
   :func: get_opts
   :prog: runeop-cml.py

job_ctrl_helpers.py
============

.. argparse::
   :filename: ../job_ctrl_helpers.py
   :func: get_opts
   :prog: job_ctrl_helpers.py
