#!/usr/bin/env python3
import os, sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock, T0FCSRLock

if __name__ == '__main__':
    t0lock = T0ProcDatasetLock(dataset='/AlCaPhiSym', stage='Repack')
    t0_fcsr_lock = T0FCSRLock()

    handler = HTCHandlerByRunDBS(task='timing-cc-rereco',
                                 dsetname='/AlCaPhiSym/*/RAW',
                                 deps_tasks=['timing-cc-val'],
                                 locks=[t0lock, t0_fcsr_lock])

    ret = handler()

    sys.exit(ret)

