#!/usr/bin/env python3
import sys
from TICMeanTimeHandler import TICMeanTimeHandler

if __name__ == '__main__':
    handler = TICMeanTimeHandler(task='timing-val',
                                 deps_tasks=['timing-reco'],
                                 prev_input='timing-reco',
                                 is_cc=False,
                                 is_rereco=False)

    ret = handler()

    sys.exit(ret)
