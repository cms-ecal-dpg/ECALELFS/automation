#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}
RUNTYPE=${8}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

sleep 10

# Run the customised HLT menu
cmsRun hlt.py inputFiles=$INFILE
RET=$?

if [ "$RET" == "0" ]
then
    # gather trigger rates

    # Monitor different paths for HI, ppRef, and pp runs
    # Path names without "_vXX" suffix
    if [[ $INFILE == *"/store/hidata/"* ]]; then
      pathToMonitor=("HLT_HIEle20Gsf"
                     "HLT_HIGEDPhoton30"
                    )
    elif [[ $RUNTYPE == "pref" ]]; then
      pathToMonitor=("HLT_PPRefEle20Gsf"
                     "HLT_PPRefGEDPhoton30"
                    )
    else
      pathToMonitor=("HLT_Ele32_WPTight_Gsf"
                     "HLT_Ele35_WPTight_Gsf"
                     "HLT_Ele38_WPTight_Gsf"
                     "HLT_Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned"
                     "HLT_Photon33"
                     "HLT_PFMET120_PFMHT120_IDTight"
                     #"HLT_DiPhoton10Time1ns"
                     "HLT_DiPhoton10Time1p2ns"
                     #"HLT_DiPhoton10Time1p4ns"
                     #"HLT_DiPhoton10Time1p6ns"
                     #"HLT_DiPhoton10Time1p8ns"
                     #"HLT_DiPhoton10Time2ns"
                     "HLT_HT430_DelayedJet40_SingleDelay0p5nsInclusive"
                     "HLT_L1Tau_DelayedJet40_DoubleDelay0p75nsInclusive"
                     "HLT_Photon50_TimeLtNeg2p5ns"
                    )
    fi

    # parse TrigReport to get the fraction of passed events
    TOTAL=`cat _condor_stdout | grep 'TrigReport Events total = ' | awk '{if ($1 == "TrigReport") print $5}'`
    INFO="total:${TOTAL}"
    for TRG in ${pathToMonitor[*]}
    do
        PASSED=`cat _condor_stdout | grep ${TRG}_v | grep 'TrigReport' | grep -v '\-----' |  awk '{if ($8 != "") print $5}'`
        INFO=$INFO" ${TRG}:${PASSED}"
    done
    # write the trigger-path to trigger-rate map into influxdb as custom fields
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields $INFO
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
