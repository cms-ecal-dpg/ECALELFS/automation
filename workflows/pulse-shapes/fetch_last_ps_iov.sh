#!/bin/bash

set -x

run_number=${1}

# get last 30 IOVs from conddb (should be enough)
iovs=`conddb list EcalPulseShapes_prompt --limit 30 2>&- | grep PulseShape | awk '{print $1}' | tac`
# search for the right IOV based on the provided run number
while IFS= read -r iov
do
    if [[ $run_number > $iov ]]; then
        theiov=$iov; break
    fi
done < <(printf '%s\n' "$iovs")

# dump content of the selected IOV
conddb_dumper -O EcalPulseShapes -t EcalPulseShapes_prompt -b $theiov -n 1 -o dump.dat
cat dump_1.dat | sed -e "s/^/0 /1" | awk '{ s = ""; for (i = 1; i <= 4; i++) s = s $i " "; p = ""; for (j = 5; j < NF; j++) p = p $j " ";  print s$17" 0 "p }' > ref.txt

rm *dat
