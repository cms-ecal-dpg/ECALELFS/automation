#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock, T0FCSRLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    t0lock = T0ProcDatasetLock(dataset='/AlCaPhiSym', stage='Repack')
    #t0_fcsr_lock = T0FCSRLock() # temporary remove FCSR lock for first collisions

    handler = HTCHandlerByRunDBS(task='pulse-shapes',
                                 dsetname='/AlCaPhiSym/*/RAW')
                                 #locks=[t0lock, t0_fcsr_lock]) # temporary remove locks for first collisions
    
    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
    
