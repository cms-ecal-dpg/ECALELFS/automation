import os
import sys
import glob as glob
import subprocess
import operator, itertools
from typing import Optional, Dict, List
from ecalautoctrl import HandlerBase, JobCtrl, process_by_intlumi
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

lumi_target = 2000
@process_by_intlumi(target = lumi_target, nogaps = False)
class EFWeightsHandler(HandlerBase):
    """
    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                task: str,
                prev_input: str,
                lumi_target: str,
                deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if not deps_tasks:
            deps_tasks = [prev_input]


        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.lumi_target = lumi_target

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')
        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def all_files_in(self, folder_url, ftype = ''):
        files_list =  glob.glob(folder_url+'/*'+ftype)
        return ','.join(files_list)

    def findFilesEopruns(self, runs):
        """_summary_

        Args:
            runs (list): a list of run numbers
            rctrl (_type_): the RunCtrl object for the relevant database and campaign

        Returns:

        """

        ecalelf = set()
        selected_list = []
        ecalelf.update(self.rctrl.getOutput(runs = runs, process='ecalelf-ntuples-wskim'))
        ecalelf.update(self.rctrl.getOutput(runs = runs, process='ecalelf-ntuples-zskim'))
        ecalelf.update(self.rctrl.getOutput(runs = runs,  process='ecalelf-ntuples'))
        for ntuples in ecalelf:
            for ntuple in ntuples.split(','):
                if "/ntuple_" in ntuple:
                    selected_list.append(ntuple)
        return selected_list

    def find_files(self, runs):
        phisym_files = {'PhiSym': list(self.rctrl.getOutput(process='phisym-reco', runs = runs))}
        eop_files = self.findFilesEopruns(runs)
        return (phisym_files, eop_files)



    def process_data(self, groups: List[Dict]):
        """
        Execute the merging of runs and finds and plots the weights values of the detector.
        """

        for group in groups:
            self.log.info('Processing run(s): '+','.join([r['run_number'] for r in group]))
            run_numbers = [r['run_number'] for r in group]
            # gather master run details
            mrun = group[-1]
            run = mrun['run_number']
            fill = mrun['fill']
            self.jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : mrun['fill']},
                            dbname=self.opts.dbname)
            repr = (mrun[self.task] == 'reprocess')
            if not self.jctrl.taskExist() or repr:
                self.jctrl.createTask(jids=[0],
                                 recreate=repr,
                                 fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
            try:
                # set the status of the runs as processing
                self.rctrl.updateStatus(run=run, status={self.task : 'processing'})

                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                self.jctrl.running(jid=0)

                #output directorys for the weight.txt file andd th plots

                runs_lst = ','.join(map(str, run_numbers))
                subdir = f'/eflow/eflow-weights/{run}/'
                plots_outdir = self.opts.eosplots+subdir
                weights_outdir = f'{self.opts.eosdir}{run}/'
                plots_url = self.opts.plotsurl+subdir
                os.makedirs(weights_outdir, exist_ok=True)
                os.makedirs(plots_outdir, exist_ok=True)

                #find the files for the group
                files = self.find_files(runs=run_numbers)

                #running the reweighting
                args = self.parser.parse_args()

                ret = subprocess.run(['reweighting_processor.py', '--phisym_list', ','.join(files[0]['PhiSym']), '--ecalelf_list', ','.join(files[1]), '-o', weights_outdir,  '--eosplots', plots_outdir, '--verbosity', '1'], capture_output=False)

                if ret.returncode == 0:

                    self.jctrl.done(jid=0, fields={'output': self.all_files_in(weights_outdir, ftype = '.txt'),
                                                   'plots': plots_url})

                else:
                    self.jctrl.failed(jid=0)
            except Exception as e:
                self.jctrl.failed(jid=0)
                self.log.error(f'Failed running eflow-weights with config {self.opts.config} on runs {group[0]["run_number"]} to {run["run_number"]}: {e}')


    def submit(self):
        """
        Execute the reweighting step.
        Reprocess reprocessing and new runs.
        """

        self.resubmit = False
        self.process_data(groups=self.groups())

        return 0

    def resubmit(self):
        """
        resubmits runs with failed Jobs
        """

        # get runs in status failed
        runs = self.rctrl.getRuns(status = {self.task : 'failed'})
        runs += self.rctrl.getRuns(status = {self.task : 'merged'})
        runs += self.rctrl.getRuns(status = {self.task : 'processing'})

        # group the runs by integrated luminosity
        lumi_groups = [[]]
        group_lumi = 0
        group_i = 0
        for run in runs:
            if group_lumi < self.lumi_target:
                lumi_groups[group_i].append(run)
                group_lumi += run['lumi']
            else:
                lumi_groups.append([])
                group_i +=1
                group_lumi = 0
                lumi_groups[group_i].append(run)

        
        # re-process the data by intlumi group
        if lumi_groups != [[]]:
            self.resubmit = True
            self.process_data(groups=lumi_groups)
            return 0




if __name__ == '__main__':
    handler = EFWeightsHandler(task='eflow-weights',
                                deps_tasks=["ecalelf-ntuples-wskim", "ecalelf-ntuples-zskim", "phisym-reco"],
                                prev_input="phisym-reco",
                                lumi_target = lumi_target)


    ret = handler()


    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EFWeightsHandler)
