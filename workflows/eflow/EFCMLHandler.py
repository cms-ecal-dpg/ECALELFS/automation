import os
import sys
import re
import numpy as np
import glob as glob
import subprocess
import operator, itertools
from typing import Optional, Dict, List
from ecalautoctrl import HandlerBase, JobCtrl, process_by_intlumi
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

lumi_target = 2000
@process_by_intlumi(target = lumi_target, nogaps = False)
class EFCMLHandler(HandlerBase):
    """
    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                task: str,
                prev_input: str,
                lumi_target: str,
                deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        if not deps_tasks:
            deps_tasks = [prev_input]


        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.lumi_target = lumi_target

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')
        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def get_past_runs(self, frun: int):
        runs = self.rctrl.getRuns(status = {'eflow-eflow': 'done'})
        runs = np.array([int(run['run_number']) for run in runs])
        #remove any data 
        runs = np.delete(runs, np.where(frun < runs))
        return list(runs)
    
    def order_files(self, history_files: set):
        nums = {}
        for file in history_files:
            #looks for a string containing letters and /s followed by six digits followed by history.root
            #to extract the 6 digit run number out of the path
            s_match = re.search(r"[\w/]+?(\d{6})[\/]+history\.root", file)
            if s_match is None:
                continue
            nums[int(s_match.group(1))] = file
        f_runs = sorted(nums.keys())
        #make sure the history files are returned in the order of increasing run numbers
        return [nums[f_run] for f_run in f_runs]
            
    def process_data(self, groups: List[Dict]):
        """
        Execute the merging of runs and finds and plots the eflow values from eflow-eflow.
        """
        for group in groups:
            self.log.info('Processing run(s): '+','.join([r['run_number'] for r in group]))
            run_numbers = [r['run_number'] for r in group]
            # gather master run details
            mrun = group[-1]
            run = mrun['run_number']
            fill = mrun['fill']
            self.jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : mrun['fill']},
                            dbname=self.opts.dbname)
            repr = (mrun[self.task] == 'reprocess')
            if not self.jctrl.taskExist() or repr:
                self.jctrl.createTask(jids=[0],
                                 recreate=repr,
                                 fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
            try:
                # set the status of the runs as processing
                self.rctrl.updateStatus(run=run, status={self.task : 'processing'})

                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                self.jctrl.running(jid=0)

                #output directorys for the output file andd th plots

                runs_lst = ','.join(map(str, run_numbers))
                subdir = f'/eflow/eflow-cml/{run}/'
                plots_outdir = self.opts.eosplots+subdir
                file_outdir = f'{self.opts.eosdir}{run}/'
                plots_url = self.opts.plotsurl+subdir
                os.makedirs(file_outdir, exist_ok=True)
                os.makedirs(plots_outdir, exist_ok=True)
                
                past_runs = self.get_past_runs(int(run))
                #find the files for all the runs where eflow-eflow has been completed
                files = self.order_files(self.rctrl.getOutput('eflow-eflow', runs = [int(run) for run in past_runs]))

                args = self.parser.parse_args()

                ret = subprocess.run(['eflow_hindsight.py', '--history_files', ','.join(files), '--eosdir', file_outdir, '--plotsdir', plots_outdir, '--verbosity', '1'], capture_output=False)

                if ret.returncode == 0:

                    self.jctrl.done(jid=0, fields={'output': f'{file_outdir}results_{run}.pkl',
                                                   'plots': plots_url})

                else:
                    self.jctrl.failed(jid=0)
            except Exception as e:
                self.jctrl.failed(jid=0)
                self.log.error(f'Failed running eflow-cml on runs {group[0]["run_number"]} to {run["run_number"]}: {e}')


    def submit(self):
        """
        Execute the data merging and plotting step.
        Reprocess reprocessing and new runs.
        """

        self.resubmit = False
        self.process_data(groups=self.groups())

        return 0


    def resubmit(self):
        """
        resubmits runs with failed Jobs
        """

        # get runs in status failed
        runs = self.rctrl.getRuns(status = {self.task : 'failed'})
        runs += self.rctrl.getRuns(status = {self.task : 'merged'})
        runs += self.rctrl.getRuns(status = {self.task : 'processing'})

        # group the runs by integrated luminosity
        lumi_groups = [[]]
        group_lumi = 0
        group_i = 0
        for run in runs:
            if group_lumi < self.lumi_target:
                lumi_groups[group_i].append(run)
                group_lumi += run['lumi']
            else:
                lumi_groups.append([])
                group_i +=1
                group_lumi = 0
                lumi_groups[group_i].append(run)

        
        # re-process the data by intlumi group
        if lumi_groups != [[]]:
            self.resubmit = True
            self.process_data(groups=lumi_groups)
            return 0




if __name__ == '__main__':
    handler = EFCMLHandler(task='eflow-cml',
                                deps_tasks=["eflow-eflow"],
                                prev_input="eflow-eflow",
                                lumi_target = lumi_target)


    ret = handler()


    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EFCMLHandler)