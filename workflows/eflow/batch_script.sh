#!/bin/bash

CLUSTERID=${1}
JOBID=${2}
TASK=${3}
FILES=${4}
PLOTSDIR=${5}
FILEDIR=${6}
weights=${7}
WDIR=${8}


source /home/ecalgit/setup.sh

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -
echo $TASK jobctrl --running --id $JOBID --fields "htc-id:${CLUSTERID}"
ecalautomation.py  $TASK jobctrl --running --id $JOBID --fields "htc-id:${CLUSTERID}"

mkdir output/

export PYTHON3PATH=$PYTHON3PATH:$(pwd)


# Run the Python script
eflow_processor.py --files_list $FILES -o $FILEDIR --plotsdir $PLOTSDIR --savePlots --verbosity 0 --weights_dir $weights --nhits '-1'
RET=$?

echo 'processor complete' $RET

if [ "$RET" == "0" ]
then
    OFILE=$FILEDIR/history.root
    ecalautomation.py $TASK jobctrl --done --id $JOBID --fields "output:${OFILE}"
    echo 'job set to done with output ' $OFILE
else
    ecalautomation.py $TASK jobctrl --failed --id $JOBID
    echo 'job set to failed'
fi

exit $RET