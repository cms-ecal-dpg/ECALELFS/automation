#!/usr/bin/env python3
import os, sys
import subprocess, shlex
import operator, itertools
from typing import Optional, Dict, List
from ecalautoctrl import HTCHandler, JobCtrl, prev_task_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase, process_by_intlumi

lumi_target = 2000
@prev_task_data_source
@process_by_intlumi(target=lumi_target, nogaps=False)
class EFlowHandlerCondor(HTCHandler):
    """
    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 lumi_target: float=0,
                 **kwargs):

        if not deps_tasks:
            deps_tasks = [prev_input]

        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.prev_input = prev_input
        self.lumi_target = lumi_target

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')
        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def get_weights(self, runs):
        weights = self.rctrl.getOutput(process = 'eflow-weights', runs = runs)
        if len(weights) == 0:
            self.log.info(f'there is no weights file for that set of runs: runs = {runs}')
            assert False
        else:
            self.log.info(f'picking the weights file {list(weights)[0]} out of the options {list(weights)}')
            weights = list(weights)[0]
        return weights


    def submit_jobs(self, groups: List[Dict], resubmit: bool=False):
        """
        Submit condor job
        """

        for group in groups:
            # master run (the final run of the group)
            mrun = group[-1]

            fdict = self.get_files(group)
            #^ this calls the get_files_prev_task(self, runs) function from TaskHandlers.py and gets the weights and phisym files

            if fdict is not None and len(fdict)>0:  #checks that data exists for the previous step

                mrun_number = mrun["run_number"]
                fill = mrun["fill"]
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number':mrun_number, 'fill':fill},
                                dbname=self.opts.dbname)

                # check for evicted jobs. Jobs still marked as running in the db but not
                # actually running in condor.
                for jid in jctrl.getRunning():
                    if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                        self.log.info(f"Found job {jid} of fill {fill} marked as running but not listed on condor. Setting job status to failed.")
                        jctrl.failed(jid=jid)

                # allow reprocessing only if previous jobs are not running
                allow_repr = (mrun[self.task] == 'reprocess')
                if jctrl.taskExist() and mrun[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        self.log.info(f"Reprocessing of run {mrun_number} not allowed because {len(jobs['idle']+jobs['running'])} job(s) are in 'idle' or 'running' state.")

                        allow_repr = False

                failedjobs = jctrl.getFailed()

                if not jctrl.taskExist() or len(failedjobs) > 0 or allow_repr:
                    condor_resubmit_args = []
                    if len(failedjobs) > 0 and not allow_repr:
                        self.log.info(f'Found {len(failedjobs)} failed job(s) for fill {fill}')
                        if resubmit:
                            condor_resubmit_args = ['-append', f'+JobFlavour = "{self.opts.resubflv}"']

                    runs = []
                    for r in group:
                        runs.append(r["run_number"])
                    run_nums = ','.join(runs)

                    plt_subdir = f'/eflow/eflow-eflow/{mrun_number}/'
                    eosplots = self.opts.eosplots+plt_subdir
                    plotsurl = self.opts.plotsurl+plt_subdir
                    os.makedirs(eosplots, exist_ok=True)


                    fileset = ','.join(list(self.rctrl.getOutput(process='phisym-reco', runs = runs)))
                    task = f"\'-w {self.task} -c {self.campaign} -t run_number:{mrun_number},fill:{fill} --db {self.opts.dbname}\'"
                    args = [task, str(fileset), eosplots, f'{self.opts.eosdir}{mrun_number}/', self.get_weights(runs = runs), self.opts.wdir]
                    args = " ".join([arg for arg in args])

                    condor_submit_cmd = ['condor_submit',
                                        f'{self.opts.template}',
                                        '-spool',
                                        '-queue', '1',
                                        '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) $(fname) {args}"']



                    ret = subprocess.run(condor_submit_cmd + condor_resubmit_args, capture_output=True)

                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info('Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {fill}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch'
                        if not resubmit:
                            try:
                                # create task injecting further job info:
                                # - group: other runs processed by this task
                                # - htc-id: HTCondor job ID
                                # - log file
                                jctrl.createTask(jids=[0],
                                                 recreate=allow_repr,
                                                 fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                          'htc-id': f"{cluster}.0",
                                                          'log': f'{logurl}/{self.task}-{cluster}-0.log',
                                                          'plots': plotsurl}])


                            except Exception as ex:
                                self.log.error(f'{type(ex).__name__}:\n{ex}')
                                self.log.warning('Automation JobCtrl failed to create task for run(s): ' + ','.join([r['run_number'] for r in group]) + f' fill {fill}. Marking all jobs of cluster {cluster} for removal.')
                                self.log.info(subprocess.run(['condor_rm', f'{cluster}'], capture_output=True).stdout.decode().strip())
                                return -1

                            # set the master run to status processing
                            self.rctrl.updateStatus(run=mrun_number, status={self.task: 'processing'})
                            if len(group) > 1:
                                # set status merged for all other runs.
                                for r in group[:-1]:
                                    self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                        else:
                            # set the job status to idle and update the htc-id and log file path
                            for jid in failedjobs:
                                jctrl.idle(jid=jid, fields={'htc-id': f"{cluster}.0", 'log': f'{logurl}/{self.task}-{cluster}-0.log'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1


        return 0

    def submit(self):
        """
        Merge the new and reprocessing.
        """

        self.submit_jobs(groups=self.groups())

        return 0

    def resubmit(self):
        """
        Resubmit runs with failed jobs
        """
        # get runs in status processing and merged
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})
        runs += self.rctrl.getRuns(status = {self.task : 'merged'})
        
        #group the runs by fill
        lumi_groups = [[]]
        group_lumi = 0
        group_i = 0
        for run in runs:
            if group_lumi < self.lumi_target:
                lumi_groups[group_i].append(run)
                group_lumi += run['lumi']
            else:
                lumi_groups.append([])
                group_i +=1
                group_lumi = 0
                lumi_groups[group_i].append(run)

        # re-process the data by intlumi group
        if lumi_groups != [[]]:
            self.submit_jobs(groups=lumi_groups, resubmit=True)
            return 0



if __name__ == '__main__':
    handler = EFlowHandlerCondor(task='eflow-eflow',
                                deps_tasks=["phisym-reco", "eflow-weights"],
                                prev_input='eflow-weights',
                                lumi_target = lumi_target)


    ret = handler()


    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EFlowHandlerCondor)
