from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: phisym", filename="phisym-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("zee-mon-fill", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        runmon = singleJob("ZeeMonHandler \n @prev_task_data_source \n @process_by_fill")
        mon_files = file("runmon.py \n zeemon_tools.py")
    with Cluster("zee-mon-cumulative", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        runcml = singleJob("ZeeMonHandler \n @prev_task_data_source \n @process_by_fill")
        cml_files = file("runcml.py \n zeemon_tools.py")

    runmon >> runcml
