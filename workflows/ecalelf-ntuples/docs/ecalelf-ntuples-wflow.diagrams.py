from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: ecalelf-ntuples", filename="ecalelf-ntuples-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("ecalelf-wskim", direction="LR"):
        Node("", width='1.3', height='0', style='invisible')
        wskim = multiJob("HTCHandlerByRunDBS \n dsetname=/EGamma/*EcalUncalWElectron-Prompt*/ALCARECO")
        wskim_locks = locks("T0ProcDatasetLock \n dataset=/EGamma \n stage=PromptReco")
        file("run-wskim.py \n ecalelf-dumper.py \n template.sub \n batch_script.sh")
    with Cluster("ecalelf-zskim", direction="LR"):
        Node("", width='1.3', height='0', style='invisible')
        zskim = multiJob("HTCHandlerByRunDBS \n dsetname=/EGamma/*EcalUncalZElectron-Prompt*/ALCARECO")
        zskim_locks = locks("T0ProcDatasetLock \n dataset=/EGamma \n stage=PromptReco")
        file("run-zskim.py \n ecalelf-dumper.py \n template.sub \n batch_script.sh")

    wskim_locks >> wskim
    zskim_locks >> zskim
