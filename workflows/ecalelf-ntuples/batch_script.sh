#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}
CERTJSON=${8}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/
export EOS_MGM_URL=root://eoscms.cern.ch

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

mkdir output
cmsRun ecalelf_dumper.py type=ALCARERECO files=$INFILE outputDir="output" doTree=1 doExtraCalibTree=1 doExtraStudyTree=1 doEleIDTree=1 doTreeOnly=0 jid=$JOBID maxEvents=-1 globalTag=$GT jsonFile=$CERTJSON
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    eos mkdir -p $EOSDIR
    LIST=$(ls output/*root)
    OFILE=""
    RETCOPY=0
    for file in ${LIST}; do
        xrdcp -f ${file} $EOS_MGM_URL/$EOSDIR/
        RETCOPY=$((RETCOPY+$?))
        OFILE="${OFILE},${EOSDIR}/"`basename ${file}`        
    done
    OFILE="${OFILE:1}"
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
